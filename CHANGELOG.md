# Changelog

## 3.2.0 - 2025-11-26

### Added

- spip/spip#6043 Adaptation minimale au nouveau formalisme d'appel des fonctions de `ajaxCallback.js`
- Installable en tant que package Composer

### Changed

- Compatible SPIP 5.0.0-dev

### Fixed

- spip/spip#5460 Utiliser les propriétés logiques dans la CSS de l'espace privé
- !4794 Diverses corrections sur les RSS des forums

### Removed

- !4795 4 fichiers de langues obsolètes et non traduits.
